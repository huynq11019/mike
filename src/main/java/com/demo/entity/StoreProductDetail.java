package com.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

@Data
@Table(name = "stockProduct_detail")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class StoreProductDetail extends AbstractAuditingEntity implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "productId")
	private Integer product_id;
	
	@Column(name = "stockId")
	private Integer stock_id;
	
	@Column(name = "quantity")
	private String quantity;
	
	
	

}
