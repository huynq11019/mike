package com.demo.entity;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;


@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractAuditingEntity implements Serializable {

	@CreatedDate
	@Column(name = "createDate", updatable = false)
	private Instant createDate = Instant.now();
	
	@CreatedBy
	@Column(name = "createBy", updatable = false)
	private Long createBy;
	
	@Column(name = "lastUpdate")
	@LastModifiedDate
	private Instant lastUpdate = Instant.now();
	
	@Column(name = "updateBy")
	private Long updateBy ;
	
// status -1 là nháp, 0 xóa, 1 active
	@Column(name = "status")
	private Integer status ;

}
