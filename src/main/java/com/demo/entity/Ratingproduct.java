package com.demo.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ratingproduct  extends AbstractAuditingEntity implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "user_rate")
	private Long userRateing;

	@Column(name = "poduct_rate")
	private Long rateProduct;

	@Column(name = "point")
	private Integer point;

	@Column(name = "rate_content")
	private String rateContent;
}
