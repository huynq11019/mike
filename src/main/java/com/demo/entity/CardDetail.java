package com.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

@Data
@Table(name = "card_detail")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class CardDetail extends AbstractAuditingEntity implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "username")
	private String username;
	
	@Column(name = "productId")
	private String product_id;
	
	@Column(name = "quantity")
	private Integer quantity;
	
	

}
