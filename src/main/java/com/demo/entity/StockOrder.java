package com.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Table(name = "stock_order")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class StockOrder extends AbstractAuditingEntity implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "stockName")
	private String stock_name;
	
	@Column(name = "address")
	private String address;
	
	

}
