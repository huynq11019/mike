package com.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Empployee_role")
public class Role extends AbstractAuditingEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private Long id;

    @Column(name = "roleCode", updatable = false)
    private String roleCode;

    @Column(name = "roleName")
    private String roleName;

    @Column(name = "description")
    private String description;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "role_authro", joinColumns = {
            @JoinColumn(name = "role_id", referencedColumnName = "role_id") }, inverseJoinColumns = {
            @JoinColumn(name = "authrority_id", referencedColumnName = "authrority_id") })
    @BatchSize(size = 20)
    private List<Authorities> authorities;
}
