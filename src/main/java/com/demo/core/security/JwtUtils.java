package com.demo.core.security;

import com.demo.core.Utils.StringPool;
import com.demo.core.Utils.Validator;
import com.demo.core.config.AuthenticationProperies;
import com.demo.core.constants.SecurityConsstants;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.*;

@Service
@Slf4j
public class JwtUtils {
    /**
     * @author: huynq
     * @since: 9/12/2021 3:26 AM
     * @description: xử lý token 2
     * @update:
     */
    // thực hiện validate
    private String secret;

    private long jwtExpirationInMs = SecurityConsstants.JWTEXPỈATIONINMS;

    private long refreshExpirationDateInMs = SecurityConsstants.REFRESH_JWTEXPỈATIONINMS;
    private AuthenticationProperies authenticationProperies;

    @Value("${jwt.secret}")
    public void setSecret(String secret) {
        this.secret = secret;
    }

    /**
     * @author: huynq
     * @since: 9/12/2021 12:45 PM
     * @description: Thực hiện tạo token
     * @update:
     */
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();

        Collection<? extends GrantedAuthority> roles = userDetails.getAuthorities();

        if (roles.contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
            claims.put("isAdmin", true);
        }
        if (roles.contains(new SimpleGrantedAuthority("ROLE_USER"))) {
            claims.put("isUser", true);
        }

        return doGenerateToken(claims, userDetails.getUsername());
    }

    private String doGenerateToken(Map<String, Object> claims, String subject) {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

        return Jwts.builder().setClaims(claims).setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS256, secret).compact();

    }

    /**
     * @author: huynq
     * @since: 9/12/2021 3:27 AM
     * @description: thực hiện tạo refresh token
     * @update:
     */
    public String doGenerateRefreshToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + refreshExpirationDateInMs))
                .signWith(SignatureAlgorithm.HS512, secret).compact();

    }

    /**
     * @author: huynq
     * @since: 9/13/2021 1:06 AM
     * @description: Create refresh token của haith
     * @update:
     */
    public JwtToken createRefreshToken(Authentication authentication) {
        // chưa truyền claims
        Map<String, Object> claims = new HashMap<>();
        return this.createRefreshToken(claims, authentication.getName());
    }

    /**
     * @author: huynq
     * @since: 9/13/2021 1:32 AM
     * @description: Phần này là refesh token của anh haith
     * @update:
     */
    public JwtToken createRefreshToken(Map<String, Object> claims, String username) {
//        String refreshToken = HMACUtil.hashSha256(UUID.randomUUID().toString() + username);

//        Date dateAfter = DateUtils.getSecondAfter(new Date(), 86400);
        Date exDate = new Date(System.currentTimeMillis() + refreshExpirationDateInMs);
        String refreshToken = Jwts.builder().setClaims(claims)
                .setSubject(username)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(exDate)
                .signWith(SignatureAlgorithm.HS512, secret).compact();
        return JwtToken.builder()
                .token(refreshToken)
                .tokenExpDate(exDate)
                .createdDate(new Date(System.currentTimeMillis()))
                .userName(username).build();

    }

    private String getHashKey(String username) {
        return Validator.isNotNull(username) ? DigestUtils.md5DigestAsHex(username.getBytes()) : StringPool.BLANK;
    }

    public Optional<JwtToken> refreshToken(String username, String refreshToken) {
        String hashKey = this.getHashKey(username);
        // Check refresh token
//        Optional<Object> rfToken = Optional.ofNullable(
//                this.redisService.hget(this.getRedisKey(username, hashKey), SecurityConstants.Jwt.REFRESH_TOKEN));

//        if (rfToken.isPresent()) {
//            return Optional.ofNullable(this.doGenerateRefreshToken(null, username));
//        }

        return Optional.empty();
    }
    /**
     * @author: huynq
     * @since: 9/13/2021 1:47 AM
     * @description: get Refresh token
     * @update:
     * */

    /**
     * @author: huynq
     * @since: 9/12/2021 3:28 AM
     * @description: Thực hiện validate token
     * @update:
     */
    public boolean validateToken(String authToken) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(authToken);
            return true;
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
            return false;
            // throw new InvalidTokenRequestException("JWT", authToken, "Malformed jwt token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
            return false;
            // throw new InvalidTokenRequestException("JWT", authToken, "Token expired. Refresh required");
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
            return false;
            // throw new InvalidTokenRequestException("JWT", authToken, "Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
            return false;
            // throw new InvalidTokenRequestException("JWT", authToken, "Illegal argument token");
        } catch (Exception e) {
            log.error("Invalid JWT signature.", e);
            return false;
        }
    }

    /**
     * @author: huynq
     * @since: 9/12/2021 3:28 AM
     * @description: Lấy thông tin user đã test vào từ Access token
     * @update:
     */
    public String getUsernameFromToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        return claims.getSubject();

    }

    /**
     * @author: huynq
     * @since: 9/12/2021 3:29
     * @description: Get danh sach các role cho  accesstoekn
     * @update:
     */
    public List<SimpleGrantedAuthority> getRolesFromToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();

        List<SimpleGrantedAuthority> roles = null;

        Boolean isAdmin = claims.get("isAdmin", Boolean.class);
        Boolean isUser = claims.get("isUser", Boolean.class);

        if (isAdmin != null && isAdmin) {
            roles = Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }

        if (isUser != null && isAdmin) {
            roles = Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
        }
        return roles;
    }

    /**
     * @author: huynq
     * @since: 9/13/2021 1:31 AM
     * @description: Thực hiện hủy token khi đăng xuất
     * @update:
     */
    public void invalidateToken() {
        Optional<UserPrincipal> userDetails = SecurityUtils.geUserDetails();
        log.info(userDetails.toString());
    }

    /**
     * @author: huynq
     * @since: 9/13/2021 1:30 AM
     * @description: Thực hiện kiểm tra xem token còn được hoặc động không
     * @update:
     */
    public boolean isTokenInBlackList(String userName, String token) {
        return false;
    }
}
