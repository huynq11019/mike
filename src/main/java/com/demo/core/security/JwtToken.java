package com.demo.core.security;

import lombok.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class JwtToken implements Serializable {
    /** The Constant serialVersionUID */
    private static final long serialVersionUID = 1L;

    /** The token. */
    private String token;


    /** The token exp date. */
    private Date tokenExpDate;

    /** The created by. */
    private Long createdBy;

    /** The created date. */
    private Date createdDate;

    /** The updated by. */
    private Long updatedBy;

    /** The updated date. */
    private Instant updatedDate;

    /** The user id. */
    private Long userId;

    /** The user name. */
    private String userName;
}
