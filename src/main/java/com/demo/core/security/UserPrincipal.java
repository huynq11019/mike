package com.demo.core.security;

import java.io.Serializable;
import java.util.Collection;
import java.util.Locale;

import com.demo.entity.Employee;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class UserPrincipal implements UserDetails, Serializable {

	
	  /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The users. */
     private Employee users;
	/** The password. */
	private transient String password;

	/** The status. */
	private transient int status;

	/** The user id. */
	private Long userId;

	/** The username. */
	private String username;

	/** The full name */
	private String fullName;

	/** The email. */
	private String email;


    /** The authorities. */
    private Collection<SimpleGrantedAuthority> authorities;

    	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return email;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}
