package com.demo.core.security.handler;

import java.io.IOException;
import java.time.Instant;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.demo.core.message.LabelKey;
import com.demo.core.message.Labels;
import com.demo.entity.UserLogin;
import com.demo.service.UserLoginService;

@Component
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler{
	  /** The user login service. */
    @Autowired
    private UserLoginService userLoginService;
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
		    Authentication authentication) throws IOException, ServletException {
        // Save authentication failure to user login table
        UserLogin loginLog = UserLogin.builder().username(authentication.getName()).ip(request.getRemoteAddr())
                .loginTime(Instant.now()).success(true).description(Labels.getLabels(LabelKey.LOGIN_SUCCESSFUL))
                .build();

        this.userLoginService.save(loginLog);
		
	}
		
	
}
