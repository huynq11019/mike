package com.demo.core.config;

import com.demo.core.security.CustomAuthenticationFilter;
import com.demo.core.security.CustomUserDetailsService;
import com.demo.core.security.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


import java.util.Collections;

@Configuration
@EnableWebSecurity
//@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
//@Import(SecurityProblemSupport.class)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AuthenticationProperies ap;

    /**
     * The Constant IGNOR_URLS.
     */
    private static final String[] IGNOR_URLS = {"/app/**/*.{js,html}", "/i18n/**", "/content/**", "/test/**"};

    /**
     * The Constant PUBLIC_URLS.
     */
    private static final String[] PUBLIC_URLS = { //
            "/api/file/download/*", //
            "/api/file/download-example/*", //
            "/api/login", //
            "/api/getNewAccessToken", //
            "/api/logout", //
            "/api/oauth/verifyAuthorizationCode",//
            "/api/file/downloadFile/*",
            "/api/file/uploadFile2",//
            "/api/user/export",
            "/api/user/change-password",
            "swagger-ui.html#/"
    };

    /**
     * The Constant PUBLIC_POST_URLS.
     */
    private static final String[] PUBLIC_POST_URLS = {};

    /**
     * The Constant PUBLIC_GET_URLS.
     */
    private static final String[] PUBLIC_GET_URLS = {};

    /**
     * The Constant AUTHENTICATED_URLS.
     */
    private static final String[] AUTHENTICATED_URLS = {"/api/**"};


    @Autowired
    private CustomUserDetailsService userDetailsService;


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // quản lý dữ liệu người dùng
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());

    }

    @Bean
    public CustomAuthenticationFilter jwtAuthenticationFilter() {
        return new CustomAuthenticationFilter();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**").antMatchers(IGNOR_URLS);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CorsConfigurationSource configCors() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        CorsConfiguration config = new CorsConfiguration();

        config.setAllowedHeaders(Collections.singletonList(ap.getAllowedHeaders()));
        config.setAllowedMethods(Collections.singletonList(ap.getAllowedMethods()));
        config.addAllowedOrigin(ap.getAllowedOrigins());
        config.setExposedHeaders(Collections.singletonList(ap.getExposedHeaders()));
        config.setAllowCredentials(ap.isAllowCredentials());
        config.setMaxAge(ap.getMaxAge());
        if (config.getAllowedOrigins() != null && !config.getAllowedOrigins().isEmpty()) {
            source.registerCorsConfiguration("/api/**", config);
        }

        return source;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http.cors().and()
                .csrf().disable();
        http
                .headers()
                .referrerPolicy(ReferrerPolicyHeaderWriter.ReferrerPolicy.STRICT_ORIGIN_WHEN_CROSS_ORIGIN)
                .and()
                .featurePolicy("geolocation 'none'; midi 'none'; sync-xhr 'none'; microphone 'none'; camera 'none'; magnetometer 'none'; gyroscope 'none'; speaker 'none'; fullscreen 'self'; payment 'none'")
                .and()
                .frameOptions()
                .deny();
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()

//                .antMatchers("/**").permitAll()
                .antMatchers(("/api/account")).authenticated()
                .antMatchers("/**").permitAll()
                .and().httpBasic()// .and().apply(this.securityConfigurerAdapter())
                .and().formLogin()//
                .disable().httpBasic().disable().logout().disable();

        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

}
