/*
 * FastDateFormatFactoryUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.demo.core.Utils;

import java.text.Format;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.lang3.time.FastDateFormat;
/**
 * The Class FastDateFormatFactoryUtil.
 */
public class FastDateFormatFactoryUtil {

    /** The Constant FULL. */
    public static final int FULL = 0;

    /** The Constant LONG. */
    public static final int LONG = 1;

    /** The Constant MEDIUM. */
    public static final int MEDIUM = 2;

    /** The Constant SHORT. */
    public static final int SHORT = 3;

    /** The date formats. */
    private static Map<String, Format> _dateFormats = new ConcurrentHashMap<>();

    /** The date time formats. */
    private static Map<String, Format> _dateTimeFormats = new ConcurrentHashMap<>();

    /** The locale. */
    private static Locale _locale = new Locale("vi", "VN");

    /** The simple date formats. */
    private static Map<String, Format> _simpleDateFormats = new ConcurrentHashMap<>();

    /** The time formats. */
    private static Map<String, Format> _timeFormats = new ConcurrentHashMap<>();

    /**
     * Gets the date.
     *
     * @param style
     *            the style
     * @param locale
     *            the locale
     * @param timeZone
     *            the time zone
     * @return the date
     */
    public static Format getDate(int style, Locale locale, TimeZone timeZone) {
        String key = getKey(style, locale, timeZone);

        Format format = _dateFormats.get(key);

        if (format == null) {
            format = FastDateFormat.getDateInstance(style, timeZone, locale);

            _dateFormats.put(key, format);
        }

        return format;
    }

    /**
     * Gets the date.
     *
     * @param locale
     *            the locale
     * @return the date
     */
    public static Format getDate(Locale locale) {
        return getDate(locale, null);
    }

    /**
     * Gets the date.
     *
     * @param locale
     *            the locale
     * @param timeZone
     *            the time zone
     * @return the date
     */
    public static Format getDate(Locale locale, TimeZone timeZone) {
        return getDate(SHORT, locale, timeZone);
    }

    /**
     * Gets the date.
     *
     * @param timeZone
     *            the time zone
     * @return the date
     */
    public static Format getDate(TimeZone timeZone) {
        return getDate(_locale, timeZone);
    }

    /**
     * Gets the date time.
     *
     * @param dateStyle
     *            the date style
     * @param timeStyle
     *            the time style
     * @param locale
     *            the locale
     * @param timeZone
     *            the time zone
     * @return the date time
     */
    public static Format getDateTime(int dateStyle, int timeStyle, Locale locale, TimeZone timeZone) {

        String key = getKey(dateStyle, timeStyle, locale, timeZone);

        Format format = _dateTimeFormats.get(key);

        if (format == null) {
            format = FastDateFormat.getDateTimeInstance(dateStyle, timeStyle, timeZone, locale);

            _dateTimeFormats.put(key, format);
        }

        return format;
    }

    /**
     * Gets the date time.
     *
     * @param locale
     *            the locale
     * @return the date time
     */
    public static Format getDateTime(Locale locale) {
        return getDateTime(locale, null);
    }

    /**
     * Gets the date time.
     *
     * @param locale
     *            the locale
     * @param timeZone
     *            the time zone
     * @return the date time
     */
    public static Format getDateTime(Locale locale, TimeZone timeZone) {
        return getDateTime(SHORT, SHORT, locale, timeZone);
    }

    /**
     * Gets the date time.
     *
     * @param timeZone
     *            the time zone
     * @return the date time
     */
    public static Format getDateTime(TimeZone timeZone) {
        return getDateTime(_locale, timeZone);
    }

    /**
     * Gets the key.
     *
     * @param arguments
     *            the arguments
     * @return the key
     */
    protected static String getKey(Object... arguments) {
        StringBuilder sb = new StringBuilder(arguments.length * 2 - 1);

        for (int i = 0; i < arguments.length; i++) {
            sb.append(arguments[i]);

            if (i + 1 < arguments.length) {
                sb.append(StringPool.UNDERLINE);
            }
        }

        return sb.toString();
    }

    /**
     * Gets the simple date format.
     *
     * @param pattern
     *            the pattern
     * @return the simple date format
     */
    public static Format getSimpleDateFormat(String pattern) {
        return getSimpleDateFormat(pattern, _locale, null);
    }

    /**
     * Gets the simple date format.
     *
     * @param pattern
     *            the pattern
     * @param locale
     *            the locale
     * @return the simple date format
     */
    public static Format getSimpleDateFormat(String pattern, Locale locale) {
        return getSimpleDateFormat(pattern, locale, null);
    }

    /**
     * Gets the simple date format.
     *
     * @param pattern
     *            the pattern
     * @param locale
     *            the locale
     * @param timeZone
     *            the time zone
     * @return the simple date format
     */
    public static Format getSimpleDateFormat(String pattern, Locale locale, TimeZone timeZone) {

        String key = getKey(pattern, locale, timeZone);

        Format format = _simpleDateFormats.get(key);

        if (format == null) {
            format = FastDateFormat.getInstance(pattern, timeZone, locale);

            _simpleDateFormats.put(key, format);
        }

        return format;
    }

    /**
     * Gets the simple date format.
     *
     * @param pattern
     *            the pattern
     * @param timeZone
     *            the time zone
     * @return the simple date format
     */
    public static Format getSimpleDateFormat(String pattern, TimeZone timeZone) {
        return getSimpleDateFormat(pattern, _locale, timeZone);
    }

    /**
     * Gets the time.
     *
     * @param style
     *            the style
     * @param locale
     *            the locale
     * @param timeZone
     *            the time zone
     * @return the time
     */
    public static Format getTime(int style, Locale locale, TimeZone timeZone) {
        String key = getKey(style, locale, timeZone);

        Format format = _timeFormats.get(key);

        if (format == null) {
            format = FastDateFormat.getTimeInstance(style, timeZone, locale);

            _timeFormats.put(key, format);
        }

        return format;
    }

    /**
     * Gets the time.
     *
     * @param locale
     *            the locale
     * @return the time
     */
    public static Format getTime(Locale locale) {
        return getTime(locale, null);
    }

    /**
     * Gets the time.
     *
     * @param locale
     *            the locale
     * @param timeZone
     *            the time zone
     * @return the time
     */
    public static Format getTime(Locale locale, TimeZone timeZone) {
        return getTime(SHORT, locale, timeZone);
    }

    /**
     * Gets the time.
     *
     * @param timeZone
     *            the time zone
     * @return the time
     */
    public static Format getTime(TimeZone timeZone) {
        return getTime(_locale, timeZone);
    }

}
