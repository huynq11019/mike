/*
 * JsonUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.demo.core.Utils;

import com.google.gson.Gson;

/**
 * The Class JsonUtil.
 */
public class JsonUtil {

    /** The Constant gSon. */
    private static final Gson gSon = new Gson();

    /**
     * From json.
     *
     * @param <T>
     *            the generic type
     * @param json
     *            the json
     * @param classOfT
     *            the class of T
     * @return the t
     */
    public static <T> T convertJsonToObject(String json, Class<T> classOfT) {
        if (Validator.isNull(json)) {
            return null;
        }

        return gSon.fromJson(json, classOfT);
    }

    /**
     * Convert to json string.
     *
     * @param obj
     *            the obj
     * @return the string
     */
    public static String convertToJsonString(Object obj) {
        if (Validator.isNull(obj)) {
            return StringPool.BLANK;
        }

        return gSon.toJson(obj);
    }
}
