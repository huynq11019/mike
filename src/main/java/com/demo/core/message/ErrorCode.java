package com.demo.core.message;

import java.util.Locale;

public enum ErrorCode {

    NA(-1, "BLANK"),

    /** The err 00000. */
    ERR_00000(0, LabelKey.SYSTEM_SUCCESSFUL),

    /** The err 00001. */
    ERR_SYSTEM_00001(1, LabelKey.SYTEM_FAILED),

    /** The err 00100. */
    ERR_INVALID_USER_OR_PASSWORD(2, LabelKey.ERROR_INVALID_USER_OR_PASSWORD),

    /** The err authen 00003. */
    ERR_LOGIN_INPUT_INVALID(3, LabelKey.ERROR_LOGIN_INPUT_INVALID),

    /** The err authen 00002. */
    ERR_INVALID_TOKEN(4, LabelKey.ERROR_INVALID_TOKEN),

    /** The error input invalid. */
    ERR_INPUT_INVALID(5, LabelKey.ERROR_INPUT_INVALID),

    /** The error role code duplicate. */
    ERR_ROLE_CODE_DUPLICATE(6, LabelKey.ERROR_ROLE_CODE_DUPLICATE),

    /** The err data is exist. */
    ERR_DATA_IS_EXIST(7, LabelKey.ERROR_CODE_DATA_IS_EXISTED),

    /** The err 00100. */
    ERR_00100(00100, LabelKey.DELETE_EMAIL_FALSE),

    /** The err 00101. */
    ERR_00101(00101, LabelKey.EMAIL_ID_NOT_EXIST),

    /** The err can not delete approval type. */
    ERR_CAN_NOT_DELETE_APPROVAL_TYPE(8, LabelKey.ERROR_CAN_NOT_DELETE_APPROVAL_TYPE),

    /** The err can not delete work flow. */
    ERR_CAN_NOT_DELETE_WORK_FLOW(9, LabelKey.ERROR_CAN_NOT_DELETE_WORK_FLOW),

    /** The err file storage failed. */
    ERR_FILE_STORAGE_FAILED(10, LabelKey.ERROR_FILE_STORAGE_FAILED),

    /** The mount is invalid. */
    MOUNT_IS_INVALID(110, LabelKey.MOUNT_IS_INVALID),

    /** The error department invalid. */
    ERROR_DEPARTMENT_INVALID(111, LabelKey.ERROR_DEPARTMENT_INVALID),

    /** The error department not exist. */
    ERR_DEPARTMENT_NOT_EXIST(112, LabelKey.ERROR_DEPARTMENT_NOT_EXIST),

    /** The error record is waiting. */
    ERROR_RECORD_IS_WAITING(113, LabelKey.ERROR_RECORD_IS_WAITING),

    /** The department not summarize. */
    DEPARTMENT_NOT_SUMMARIZE(114, LabelKey.DEPARTMENT_NOT_SUMMARIZE),

    CI_DUPLICATED(115, LabelKey.CI_DUPLICATED),

    ERR_USER_CURRENT_DOES_NOT_HAVE_PERMISSION(704, LabelKey.USER_CURRENT_DOES_NOT_HAVE_PERMISSION),

    ERR_STEP_USER_IS_EXPIRED(705, LabelKey.STEP_USER_IS_EXPIRED),

    /** The err can not delete approval type. */
    ERR_CAN_NOT_DELETE_BUDGET_PLAN(116, LabelKey.ERROR_CAN_NOT_DELETE_BUDGET_PLAN),

    /** The err criteria not exist */
    ERR_CRITERIA_NOT_EXIST(120,LabelKey.ERR_CRITERIA_NOT_EXIST),

    /** Can not update this user. */
    ERR_CAN_NOT_UPDATE_THIS_USER(140, LabelKey.ERROR_CAN_NOT_EDIT_THIS_USER),

    /** The error user not exist. */
    ERR_USER_NOT_EXIST(141,LabelKey.ERR_USER_NOT_EXIST),

    /** The error not exist. */
    ERR_NOT_EXIST(144,LabelKey.ERR_NOT_EXIST),

    /** The error is null. */
    ERR_IS_NULL(145,LabelKey.ERR_IS_NULL),

    ERROR_DATA_INVALID(146, LabelKey.ERROR_DATA_INVALID),

    ERROR_SUPPLIER_EVALUATE_SCORE_INVALID(147, LabelKey.ERROR_SUPPLIER_EVALUATE_SCORE_INVALID),

    ERROR_SUPPLIER_NOT_EXIST(148, LabelKey.ERROR_SUPPLIER_NOT_EXIST),

    ERROR_ACCESSORY_NOT_EXIST(149, LabelKey.ERROR_ACCESSORY_NOT_EXIST),

    /** The error user not permission. */
    ERR_USER_NOT_PERMISSION(150, LabelKey.ERR_USER_NOT_PERMISSION),

    ERROR_RECORD_DOES_NOT_EXIT(151, LabelKey.ERROR_RECORD_DOES_NOT_EXIT),

    ERROR_CRITERIA_NOT_EXIST(152, LabelKey.ERROR_CRITERIA_NOT_EXIST),

    ERROR_RATING_TYPE_NOT_EXIST(153, LabelKey.ERROR_RATING_TYPE_NOT_EXIST),

    /** The error is null. */
    USER_NOT_HAVE_EMAIL(154,LabelKey. USER_NOT_HAVE_EMAIL),

    /**
     * Ranking supplier error
     */
    ERROR_RANKING_LOT_NG_BIGGER_LOT(155, LabelKey.ERROR_RANKING_LOT_NG_BIGGER_LOT),

    ERROR_RANKING_IMPROVE_QPN_BIGGER_TOTAL_QPN(156, LabelKey.ERROR_RANKING_IMPROVE_QPN_BIGGER_TOTAL_QPN),

    ERROR_RANKING_FEEDBACK_TWENTY_FOUR_BIGGER_TOTAL_FEEDBACK(156, LabelKey.ERROR_RANKING_FEEDBACK_TWENTY_FOUR_BIGGER_TOTAL_FEEDBACK),

    ERROR_MONTHLY_RANKING_ALREADY_EXISTS(158, LabelKey.ERROR_MONTHLY_RANKING_ALREADY_EXISTS),

    ERROR_BADREQUEST(159, LabelKey.ERROR_BADREQUEST),
    /**
     * Vendor error
     */
    ERROR_VENDOR_DOES_NOT_EXIT(157, LabelKey.ERROR_VENDOR_DOES_NOT_EXIT)

    ;

    /**
     * Find by error code.
     *
     * @param errorCode
     *            the error code
     * @return the error code
     */
    public static ErrorCode findByErrorCode(int errorCode) {
        ErrorCode[] arrays = values();

        for (ErrorCode emotion : arrays) {
            if (emotion.geterrorCode() == errorCode) {
                return emotion;
            }
        }

        return NA;
    }

    /** The error code. */
    private final int errorCode;

    /** The message key. */
    private final String messageKey;

    /**
     * Instantiates a new error code.
     *
     * @param errorCode
     *            the error code
     * @param messageKey
     *            the message key
     */
    ErrorCode(int errorCode, String messageKey) {
        this.errorCode = errorCode;
        this.messageKey = messageKey;
    }

    /**
     * Gets the error code.
     *
     * @return the error code
     */
    public int geterrorCode() {
        return this.errorCode;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        if (this.messageKey != null ) {
            return Labels.getLabels(this.messageKey, Labels.getDefaultLocale());
        }

        return "BLANK";
    }

    /**
     * Gets the message.
     *
     * @param locale
     *            the locale
     * @return the message
     */
//    public String getMessage(Locale locale) {
//        if (Validator.isNotNull(this.messageKey)) {
//            return Labels.getLabels(this.messageKey, locale);
//        }
//
//        return StringPool.BLANK;
//    }

    /**
     * Gets the message.
     *
     * @param locale
     *            the locale
     * @param objs
     *            the objs
     * @return the message
     */
//    public String getMessage(Locale locale, Object[] objs) {
//        if (Validator.isNotNull(this.messageKey)) {
//            return Labels.getLabels(this.messageKey, objs, locale);
//        }
//
//        return StringPool.BLANK;
//    }

    /**
     * Gets the message key.
     *
     * @return the message key
     */
    public String getMessageKey() {
        return this.messageKey;
    }
}
