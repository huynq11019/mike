package com.demo.core.constants;

import java.util.Arrays;
import java.util.List;

public class Constants {
    public interface BudgetPlanSupplement {

        Integer SUPPLEMENT = 1;

    }

    public interface BudgetSupplement {

        Integer SUPPLEMENT = 1;

    }

    public static final String ROOT_FOLDER_FTP = "uploads/";

    public static final Integer LENGTH_OF_PASSWORD = 6;

    /* Ngưỡng điểm các tiêu chí đánh giá nhà cung cấp */
    List<Integer> SCORE_SUPPLIER_EVALUATE = Arrays.asList(-1, 0, 1, 2, 3, 4);

    /* Điểm tối đa của 1 tiêu chí đánh giá */
    Integer MAX_SCORE_SUPPLIER_EVALUATE = 4;

    int MAX_PERCENT = 100;

    public interface EmailTemplateCode {

        String NOTIFI_APPROVAL_APPROVED = "TBPHT";

    }

    public interface ApprovalType {

        /**
         * Ma code du chi bo phan.
         */
        String BUDGET_PLAN_PART = "DCBP";
        String BUDGET_PART = "NSBP";

        /**
         * Ma code du chi tong hop.
         */
        String BUDGET_PLAN_SUM = "DCTH";

        /**
         * Ma code du chi ke toan.
         */
        String BUDGET_PLAN_ACC = "DCKT";
        String BUDGET_ACC = "NSKT";

        /**
         * Ma code ngan sach tong hop.
         */
        String BUDGET_SUM = "NSTH";

        /**
         * Ma code du chi bo phan bo sung.
         */
        String BP_PART_SUP = "DCBPBS";
        String B_PART_SUP = "NSBPBS";

        /**
         * Ma code du chi tong hop bo sung.
         */
        String BP_SUM_SUP = "DCTHBS";
        String B_SUM_SUP = "NSTHBS";

        /**
         * Ma code du chi ke toan bo sung.
         */
        String BP_ACC_SUP = "DCKTBS";
        String B_ACC_SUP = "NSKTBS";
    }

    public interface CommitmentStatus {

        String SIGNED = "Đã ký hợp đồng/Đã cam kết";

        String ESTIMATE = "Ước tính/chưa cam kết";
    }

    public interface UserDepartmentType {

        Integer BUDGET_PLAN = 1;

        Integer BUDGET = 2;
    }

    public final static List<String> EXTENSIONS = Arrays.asList(("bmp,jpg,png,jpeg".split(",")));

    public static final String TEMP_DIR = "temp";

    public static List<String> getValidExtensions() {
        return EXTENSIONS;
    }

    public interface VinSmart {

        public static final String VINSMART = "VinSmart";

        public static final String VINSMART_CODE = "VSM";

    }

    public interface SyncType {

        /**
         * The sync sap.
         */
        String CALL_SAP_FOR_SYNC = "CALL_SAP_FOR_SYNC";

        /**
         * The sap call portal.
         */
        String SAP_CALL_PORTAL = "SAP_CALL_PORTAL";

        /**
         * The create budget to sap.
         */
        String CREATE_BUDGET_TO_SAP = "CREATE_BUDGET_TO_SAP";

        /**
         * The sync budget plan real.
         */
        String SYNC_BUDGET_PLAN_REAL = "SYNC_BUDGET_PLAN_REAL";
    }

    public interface SendMailTeamsStatus {

        public static final Integer UNSENT = 0;

        public static final Integer SENT = 1;

        public static final Integer ERROR = 2;

    }

    public interface SummaryStatus {

        public static final Integer SUM = 1;

        public static final Integer NOTSUM = 0;

        public static final Integer ACCOUNTANT = 2;

        public static final Integer REPORT_ACCOUNTANT = 2;

    }

    /**
     * <PRE>
     * The Interface BudgetStatus.</BR>
     * </PRE>
     * <p>
     * May 20, 2021 - ToanVD: Create new
     *
     * @author ToanVD
     */
    public interface BudgetStatus {

        /**
         * The Constant DRAFT.
         */
        public static final Integer DRAFT = 1;

        /**
         * The Constant WAITING.
         */
        public static final Integer WAITING = 2;

        /**
         * The Constant APPROVED.
         */
        public static final Integer APPROVED = 3;
    }

    /**
     * <PRE>
     * The Interface ApprovalStatus.</BR>
     * </PRE>
     * <p>
     * May 20, 2021 - ToanVD: Create new
     *
     * @author ToanVD
     */
    public interface ApprovalStatus {

        /**
         * The Constant GROUP_CODE.
         */
        public static final String GROUP_CODE = "";

        /**
         * The Constant CANCELED_STATUS.
         */
        public static final Integer CANCELED_STATUS = -1;

        /**
         * The Constant DRAF_STATUS.
         */
        public static final Integer DRAFT_STATUS = 0;

        /**
         * The Constant WAITING_STATUS.
         */
        public static final Integer WAITING_STATUS = 1;

        /**
         * The Constant WAITING_EDIT_STATUS.
         */
        public static final Integer WAITING_EDIT_STATUS = 2;

        /**
         * The Constant REJECTED_STATUS.
         */
        public static final Integer REJECTED_STATUS = 3;

        /**
         * The Constant APPROVED_STATUS.
         */
        public static final Integer APPROVED_STATUS = 4;

    }

    /**
     * The Interface ApprovalType.
     */
    // public interface ApprovalCategory {
    //
    // /** The Constant GROUP_CODE. */
    // public static final String GROUP_CODE = "";
    //
    // /** The Constant APPROVED_STATUS. */
    // public static final String NGAN_SACH = "BUDGET";
    //
    // /** The Constant REJECTED_STATUS. */
    // public static final String DU_CHI = "BUDGET_PLAN";
    //
    // public static final String BCKTDC = "BCKTDC";
    //
    // }

    /**
     * <PRE>
     * The Interface CfcType.</BR>
     * </PRE>
     * <p>
     * May 20, 2021 - ToanVD: Create new
     *
     * @author ToanVD
     */
    public interface CfcType {

        /**
         * The Constant COST_CENTER.
         */
        public static final String COST_CENTER = "Cost";

        /**
         * The Constant FUND_CENTER.
         */
        public static final String FUND_CENTER = "Fund";
    }

    /**
     * <PRE>
     * The Interface LanguageCode.</BR>
     * </PRE>
     * <p>
     * May 20, 2021 - ToanVD: Create new
     *
     * @author ToanVD
     */
    public interface LanguageCode {

        /**
         * The Constant ENGLISH_CODE.
         */
        public static final String ENGLISH_CODE = "en_KH";
    }

    /**
     * <PRE>
     * The Interface Pan.</BR>
     * </PRE>
     * <p>
     * May 20, 2021 - ToanVD: Create new
     *
     * @author ToanVD
     */
    public interface Pan {

        /**
         * The Constant MII.
         */
        public static final Integer MII = 8;

        /**
         * The Constant PAN_LENGTH.
         */
        public static final Integer PAN_LENGTH = 19;

        /**
         * The Constant PAN_NUMBER_START_INDEX.
         */
        public static final Integer PAN_NUMBER_START_INDEX = 6;

        /**
         * The Constant PAN_NUMBER_END_INDEX.
         */
        public static final Integer PAN_NUMBER_END_INDEX = 19;

        /**
         * The Constant ACCOUNT_NUMBER.
         */
        public static final String ACCOUNT_NUMBER = "%012d";

        /**
         * The Constant EMPTY.
         */
        public static final String EMPTY = "";

        /**
         * The Constant TELEPHONE_CODE.
         */
        public static final String TELEPHONE_CODE = "855";
    }

    /**
     * <PRE>
     * The Interface Pin.</BR>
     * </PRE>
     * <p>
     * May 20, 2021 - ToanVD: Create new
     *
     * @author ToanVD
     */
    public interface Pin {

        /**
         * The Constant PAN_LENGTH.
         */
        public static final Integer PAN_LENGTH = 20;

        /**
         * The Constant PIN_MIN_LENGTH.
         */
        public static final Integer PIN_MIN_LENGTH = 6;

        /**
         * The Constant PIN_MAX_LENGTH.
         */
        public static final Integer PIN_MAX_LENGTH = 12;

        /**
         * The Constant PIN_BLOCK_DIGIT.
         */
        public static final Integer PIN_BLOCK_DIGIT = 16;

        /**
         * The Constant PIN_RANDOM_LIST.
         */
        public static final String PIN_RANDOM_LIST = "0123456789";

        /**
         * The Constant PIN_LENGTH.
         */
        public static final Integer PIN_LENGTH = 6;

        /**
         * The Constant RANDOM_LIST_FORMAT_3.
         */
        public static final String RANDOM_LIST_FORMAT_3 = "ABCDEF";

        /**
         * The Constant FIRST_FORMAT_3.
         */
        public static final String FIRST_FORMAT_3 = "3";

        /**
         * The Constant PAN_NUMBER_START_INDEX.
         */
        public static final Integer PAN_NUMBER_START_INDEX = 6;

        /**
         * The Constant PAN_NUMBER_END_INDEX.
         */
        public static final Integer PAN_NUMBER_END_INDEX = 19;

        /**
         * The Constant CHECK_DIGIT.
         */
        public static final Integer CHECK_DIGIT = 1;

        /**
         * The Constant FIRST_PAN_PAD.
         */
        public static final String FIRST_PAN_PAD = "0000";

        /**
         * The Constant ZERO_PAD.
         */
        public static final String ZERO_PAD = "0000000000000000";

        /**
         * The Constant PIN_MAX_IN_VALID_TIMES.
         */
        public static final Integer PIN_MAX_IN_VALID_TIMES = 3;
    }

    /**
     * <PRE>
     * The Interface RedisConfig.</BR>
     * </PRE>
     * <p>
     * May 20, 2021 - ToanVD: Create new
     *
     * @author ToanVD
     */
    public interface RedisConfig {

        /**
         * The Constant DEFAULT_SECONDS_DURATION.
         */
        public static final Integer DEFAULT_SECONDS_DURATION = 300;

        /**
         * The Constant REDIS_CACHE_NAME.
         */
        public static final String REDIS_CACHE_NAME = "VSM_Redis";

        /**
         * The Constant JWT_ACCESS_TOKEN
         */
        public static final String JWT_ACCESS_TOKEN = "JWT_ACCESS_TOKEN_";

        /**
         * The Constant PERMISSION
         */
        public static final String PERMISSION = "PERMISSION_";

        /**
         * The Constant JWT_REFRESH_TOKEN
         */
        public static final String JWT_REFRESH_TOKEN = "JWT_REFRESH_TOKEN_";
    }

    /**
     * <PRE>
     * The Interface Securiry.</BR>
     * </PRE>
     * <p>
     * May 20, 2021 - ToanVD: Create new
     *
     * @author ToanVD
     */
    public interface Securiry {

        /**
         * The Constant FIRST_FORMAT_0.
         */
        public static final String FIRST_FORMAT_0 = "0";

        /**
         * The Constant PIN_PAD_FORMAT_0.
         */
        public static final String PIN_PAD_FORMAT_0 = "FFFFFFFFFFFFFF";

        /**
         * The Constant HEX_PATTERN.
         */
        public static final String HEX_PATTERN = "[0-9a-fA-F]+";

        /**
         * The Constant HEX_VALUE.
         */
        public static final String HEX_VALUE = "0123456789ABCDEF";

        /**
         * The Constant NUMERIC_PATTERN.
         */
        public static final String NUMERIC_PATTERN = "[-+]?\\d*\\.?\\d+";

        /**
         * The Constant ALGORITHM.
         */
        public static final String ALGORITHM = "SHA1PRNG";

        /**
         * The Constant SIMPLE_DATE_FORMAT.
         */
        public static final String SIMPLE_DATE_FORMAT = "dd/MM/yyyy hh:mm:ss";
    }

    /**
     * <PRE>
     * The Interface Status.</BR>
     * </PRE>
     * <p>
     * May 20, 2021 - ToanVD: Create new
     *
     * @author ToanVD
     */
    public interface Status {

        /**
         * The Constant ACTIVE.
         */
        public static final Integer ACTIVE = 1;

        /**
         * The Constant DEACTIVE.
         */
        public static final Integer DEACTIVE = 0;

        /**
         * The Constant DENY.
         */
        public static final Integer DENY = -1;

        /**
         * The Constant DISABLE.
         */
        public static final boolean DISABLE = false;

        /**
         * The Constant ENABLE.
         */
        public static final boolean ENABLE = true;

        /**
         * The Constant FAILURE.
         */
        public static final Integer FAILURE = 2;

        /**
         * The Constant SUCCESS.
         */
        public static final Integer SUCCESS = 3;
    }

    /**
     * <PRE>
     * The Interface SystemParamStatus.</BR>
     * </PRE>
     * <p>
     * May 20, 2021 - ToanVD: Create new
     *
     * @author ToanVD
     */
    public interface SystemParamStatus {

        /**
         * The Constant ACTIVE.
         */
        public static final Integer ACTIVE = 1;

        /**
         * The Constant DEACTIVE.
         */
        public static final Integer DEACTIVE = 0;

        /**
         * The Constant NEW.
         */
        public static final Integer NEW = 2;
    }

    /**
     * <PRE>
     * The Interface User.</BR>
     * </PRE>
     * <p>
     * May 20, 2021 - ToanVD: Create new
     *
     * @author ToanVD
     */
    public interface User {

        /**
         * The Constant ACTIVE.
         */
        public static final Integer ACTIVE = 1;

        /**
         * The Constant DEACTIVE.
         */
        public static final Integer DEACTIVE = 0;

        /**
         * The Constant DENY.
         */
        public static final Integer DENY = -1;

        /**
         * The Constant TEMP.
         */
        public static final Integer TEMP = -2;

        /**
         * The Constant TEMP_LOCK.
         */
        public static final Integer TEMP_LOCK = -3;
    }

    /**
     * The Interface CheckToken.
     */
    public interface CheckToken {

        /**
         * The Constant DONE.
         */
        public static final Integer DONE = 1;

        /**
         * The Constant IS_EXPIRED.
         */
        public static final Integer IS_EXPIRED = 2;

        /**
         * The Constant TOKEN_NOT_EXIST.
         */
        public static final Integer TOKEN_NOT_EXIST = 3;

        /**
         * The Constant IS_USED.
         */
        public static final Integer IS_USED = 4;

    }

    /**
     * <PRE>
     * The Interface Month.</BR>
     * </PRE>
     * <p>
     * May 20, 2021 - ToanVD: Create new
     *
     * @author ToanVD
     */
    public interface Month {

        /**
         * The Constant JAN.
         */
        public static final Integer JAN = 1;

        /**
         * The Constant FEB.
         */
        public static final Integer FEB = 2;

        /**
         * The Constant MAR.
         */
        public static final Integer MAR = 3;

        /**
         * The Constant APR.
         */
        public static final Integer APR = 4;

        /**
         * The Constant MAY.
         */
        public static final Integer MAY = 5;

        /**
         * The Constant JUN.
         */
        public static final Integer JUN = 6;

        /**
         * The Constant JUL.
         */
        public static final Integer JUL = 7;

        /**
         * The Constant AUG.
         */
        public static final Integer AUG = 8;

        /**
         * The Constant SEP.
         */
        public static final Integer SEP = 9;

        /**
         * The Constant OCT.
         */
        public static final Integer OCT = 10;

        /**
         * The Constant NOV.
         */
        public static final Integer NOV = 11;

        /**
         * The Constant DEC.
         */
        public static final Integer DEC = 12;
    }

    public interface CategoryType {

        public static final String APPROVAL = "APPROVAL";

        public static final String BUDGET = "BUDGET";

        public static final String BUDGET_PLAN = "BUDGET_PLAN";

        public static final String REPORT_BUDGET_PLAN = "BCKTDC";

        public static final String CHANGE_MANAGEMENT = "CHANGE_MANAGEMENT";

        public static final String CHANGE_MANAGEMENT_ECN = "CHANGE_MANAGEMENT_ECN";

        public static final String QUALITY_PROBLEM_SOLVING = "QUALITY_PROBLEM_SOLVING";

        public static final String ABNORMAL_HANDING = "ABNORMAL_HANDING";

        public static final String ABNORMAL_HANDING_APPROVAL = "ABNORMAL_HANDING_APPROVAL";

        public static final String ABNORMAL_HANDLING_REPORT = "ABNORMAL_HANDLING_REPORT";

        public static final String ABNORMAL_HANDLING_REPORT_APPROVAL = "ABNORMAL_HANDLING_REPORT_APPROVAL";

        public static final String SHIPPING_CONTROL = "SHIPPING_CONTROL";

        public static final String SHIPPING_CONTROL_NCC = "SHIPPING_CONTROL_NCC";

        public static final String SHIPPING_CONTROL_SQE = "SHIPPING_CONTROL_SQE";

        String EVALUATE_ISSUE = "EVALUATE_ISSUE";

        String CAUSE_ISSUE = "CAUSE_ISSUE";

        String COUNTERMEASURE = "COUNTERMEASURE";

        String SUPPLIER_EVALUATE = "SUPPLIER_EVALUATE";

        String RANKING_SUPPLIER = "RANKING_SUPPLIER";

    }

    public interface ChangeType {
        public static final String DEVIATION_REQUEST = "DR";

        public static final String DESIGN_CHANGE_REQUEST = "DCR";

        public static final String PROCESS_CHANGE_REQUEST = "PCR";

        public static final String ENGINEERING_CHANGE_NO = "ECN";
    }

    public interface ErrorTypes {
        /**
         * Lỗi ngoại quan
         */
        public static final Integer APPEARANCE = 1;

        /**
         * Lỗi kích thước
         */
        public static final Integer DIMENSION = 2;

        /**
         * Lỗi chức năng
         */
        public static final Integer FUNCTION = 3;

        /**
         * Lỗi khác
         */
        public static final Integer OTHER = 4;
    }

    public interface ErrorLevel {
        /**
         * Lỗi nghiêm trọng
         */
        public static final String CRITICAL = "CRITICAL";

        /**
         * Lỗi nặng
         */
        public static final String MAJOR = "MAJOR";

        /**
         * Lỗi nhẹ
         */
        public static final String MINOR = "MINOR";

    }

    public interface ExpiredStatus {

        public static final Integer OK = 1;

        public static final Integer WARNING = 2;

        public static final Integer DANGER = 3;
    }

    public interface StepStatus {
        /**
         * Ch? x? l�
         */
        public static final Integer WAITING = 1;
        /**
         * �ang x? l�
         */
        public static final Integer IN_PROGRESS = 2;
        /**
         * �� x? l�
         */
        public static final Integer DONE = 3;
        /**
         * T? ch?i
         */
        public static final Integer REJECTED = 4;
        /**
         * H?y
         */
        public static final Integer CANCELED = 5;
    }

    public interface AbnormalHandlingStatus {
        /**
         * Delay
         */
        public static final Integer DELAY = 1;

        /**
         * In Progress
         */
        public static final Integer IN_PROGRESS = 2;

        /**
         * Closed
         */
        public static final Integer CLOSED = 3;
    }

    public interface UserStepStatus {
        /**
         * Chua duy?t
         */
        public static final Integer WAITING = 0;
        /**
         * �� duy?t
         */
        public static final Integer APPROVED = 1;
        /**
         * T? ch?i
         */
        public static final Integer REJECT = 2;
        /**
         * supporter state
         */
        public static final Integer SUPPORTER_STATE = 3;
    }

    public interface ApprovalRole {
        /**
         * Nguuoi xu ly
         */
        public static final String ROLE_BOSS = "ROLE_BOSS";
        /**
         * Nguoi cho xu ly giup
         */
        public static final String ROLE_SUPPORTER = "ROLE_SUPPORTER";
        /**
         * Nguoi gui
         */
        public static final String ROLE_SENDER = "ROLE_SENDER";
        /**
         * Ngucho y kien
         */
        public static final String ROLE_CONSULT = "ROLE_CONSULT";
    }

    public interface Number {
        public static final Integer NEGATIVE_ONE = -1;

        public static final Integer ZERO = 0;

        public static final Integer ONE = 1;

        public static final Integer TWO = 2;

        public static final Integer THREE = 3;

        public static final Integer FOUR = 4;
    }

    public interface ROLE_CODE {

        /**
         * The Constant ROLE_CODE_BASIC.
         */
        public static final String ROLE_CODE_BASIC = "ROLE_BASIC";

        /**
         * The Constant ROLE_CODE_ADMIN.
         */
        public static final String ROLE_CODE_ADMIN = "ROLE_ADMIN";
    }

    /**
     * The Interface ApprovalType.
     */
    public interface Currency {

        /**
         * The Constant GROUP_CODE.
         */
        public static final String VND = "VND";

        /**
         * The Constant APPROVED_STATUS.
         */
        public static final String USD = "USD";

        /**
         * The Constant REJECTED_STATUS.
         */
        public static final String EUR = "EUR";

    }

    public interface SequenceGroup {

        /**
         * The Constant BUDGET_PLAN.
         */
        public static final String BUDGET_PLAN = "BUDGET_PLAN";
    }

    /**
     * The interface Gender
     */

    interface Gender {

        /**
         * Men
         */
        String MEN = "Nam";

        /**
         * Women
         */
        String WOMEN = "Nữ";
    }

    interface Version {
        Integer DEFAULT = 1;
    }

    interface TOKEN_STATUS {
        Integer RESET_PASSWORD = -2;
    }

    interface RANKING_SCORE {
        Integer INC_SCORE_MAX = 28;
        Integer RMA_SCORE_MAX = 20;
        Integer QPN_SCORE_MAX = 16; // Vấn đề phát sinh
        Integer CA_SCORE_MAX = 8; // Thái độ hợp tác
        Integer IMP_SCORE_MAX = 8; // Hiệu quả cải tiến, kiểm soát
        Integer AUDIT_SCORE_MAX = 20;
    }

    public interface CHART_TYPE {
        public static final String SHIPPING_CONTROLER = "SHIPPING_CHART";
        public static final String AUDIT_SUPPLIER = "AUDIT_SUPPLIER_CHART";
        public static final String CHANGE_DR = "CHANGE_DR_CHART";
        public static final String CHANGE_DCR = "CHANGE_DCR_CHART";
        public static final String CHANGE_PCR = "CHANGE_PCR_CHART";
        public static final String QPS = "QPS_CHART";
        public static final String RANKING_SUPPLIER = "RANKING_SUPPLIER_CHART";
        public static final String PPAP = "SHIPPING_CHART";

    }

    public interface RESPON_STATUS {

    }
}
