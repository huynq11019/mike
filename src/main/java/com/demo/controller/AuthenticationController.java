package com.demo.controller;

import com.demo.controller.protoco.request.AuthenticationRequest;
import com.demo.controller.protoco.request.EmployeeRequest;
import com.demo.controller.protoco.response.AuthenticationResponse;
import com.demo.controller.protoco.response.EmployeeResponse;
import com.demo.core.constants.SecurityConsstants;
import com.demo.core.security.CustomUserDetailsService;
import com.demo.core.security.JwtToken;
import com.demo.core.security.JwtUtils;
import com.demo.service.EmployeeService;
import io.jsonwebtoken.impl.DefaultClaims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/api")
public class AuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private CustomUserDetailsService userDetailsService;

    private static final int SALT_COUNT = 5;


    @Autowired
    private JwtUtils jwtUtil;

    @Autowired
    private EmployeeService employeeService;

    private long jwtExpirationInMs = SecurityConsstants.JWTEXPỈATIONINMS;
    private long refreshExpirationDateInMs = SecurityConsstants.REFRESH_JWTEXPỈATIONINMS;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<AuthenticationResponse> createAuthenticationToken(@RequestBody @Valid AuthenticationRequest authenticationRequest)
            throws Exception {
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
//        Date now = new Date(System.currentTimeMillis() + refreshExpirationDateInMs);
        try {
            SecurityContext sc = SecurityContextHolder.getContext();
            log.info(authenticationRequest.toString());
            UsernamePasswordAuthenticationToken authReq
                    = new UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(), authenticationRequest.getPassWord());
            Authentication auth = authenticationManager.authenticate(authReq);
            System.out.println(auth);

        UserDetails userdetails = userDetailsService.loadUserByUsername(authenticationRequest.getUserName());
        String token = jwtUtil.generateToken(userdetails);
        String userName = jwtUtil.getUsernameFromToken(token);
        // tạo refesh token
        JwtToken refreshToken = jwtUtil.createRefreshToken(auth);
        log.info(userName);
        //token
        authenticationResponse.getAuth().setToken(token);
        authenticationResponse.getAuth().setCreatedBy(Long.parseLong("-1"));
        authenticationResponse.getAuth().setCreatedDate(new Date(System.currentTimeMillis()));
        authenticationResponse.getAuth().setTokenExpDate(new Date(System.currentTimeMillis() + jwtExpirationInMs));
        authenticationResponse.getAuth().setUserName(userName);
        // refresh token
        authenticationResponse.setRefreshToken(refreshToken);
        return ResponseEntity.ok(authenticationResponse);
        } catch (DisabledException e) {

            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {

            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    @RequestMapping(value = "/createEmployee", method = RequestMethod.POST)
    public ResponseEntity<EmployeeResponse> saveUser(@RequestBody @Valid EmployeeRequest user) throws Exception {
        return ResponseEntity.ok(employeeService.createeEmployee(user));
    }

//    @RequestMapping(value = "/refreshtoken", method = RequestMethod.GET)
//    public ResponseEntity<?> refreshtoken(HttpServletRequest request) throws Exception {
//        // From the HttpRequest get the claims
//        DefaultClaims claims = (io.jsonwebtoken.impl.DefaultClaims) request.getAttribute("claims");
//
//        Map<String, Object> expectedMap = getMapFromIoJsonwebtokenClaims(claims);
//        String token = jwtUtil.doGenerateRefreshToken(expectedMap, expectedMap.get("sub").toString());
//        return ResponseEntity.ok(new AuthenticationResponse(token));
//    }

    public Map<String, Object> getMapFromIoJsonwebtokenClaims(DefaultClaims claims) {
        Map<String, Object> expectedMap = new HashMap<String, Object>();
        for (Map.Entry<String, Object> entry : claims.entrySet()) {
            expectedMap.put(entry.getKey(), entry.getValue());
        }
        return expectedMap;
    }
}
