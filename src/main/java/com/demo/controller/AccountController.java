package com.demo.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class AccountController {
    @RequestMapping("/api/account")
    public String list() {
        log.info(" Đây là Account COntroller ");
        return "hello Mike";
    }
}
