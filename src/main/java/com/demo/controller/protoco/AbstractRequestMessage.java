package com.demo.controller.protoco;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public abstract class AbstractRequestMessage {
    /** The user name. */
    protected String userName;

    /** The user id. */
    protected Long userId = Long.getLong("-1") ;
}
