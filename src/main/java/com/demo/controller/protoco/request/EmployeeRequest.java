package com.demo.controller.protoco.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class EmployeeRequest {

    private Long id;
    @NotNull
    @NotBlank(message = "User Name không được để trống")
    private String userName;
    @NotBlank
    @NotNull
    @Length(min = 8, max = 63)
    private String passwordHash;
    @NotNull
    @NotNull
    @Email
    private String email;
    @Length(min = 0, max = 60)
    private String address;
    @NotNull
    @NotBlank
    private String fullName;

    private Integer gender;

    private Date dob;

    private String avatar;
    @NotNull
    private Long role;
}
