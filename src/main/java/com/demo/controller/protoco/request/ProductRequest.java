package com.demo.controller.protoco.request;

import com.demo.controller.protoco.AbstractRequestMessage;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest extends AbstractRequestMessage {


        @NotNull(message = "id must not be empty")
        private String id;

        @NotNull(message = "product must not be empty")
        private String productName;

        private String thumbNail;

        private String subCateName;

        private Long price;

        private float discount;

        @NotNull(message = "supplierID must not be empty")
        private Long supplierId;

        private Long vendorId;

        private List<String> images;

        private List<MultipartFile> files;



}
