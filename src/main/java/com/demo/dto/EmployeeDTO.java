package com.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class EmployeeDTO {

    private Long id;
    private String userName;
    private String passwordHash;
    private String email;
    private String address;
    private String fullName;
    private Integer gender;
    private Date dob;
    private String avatar;
    private Long role;
}
