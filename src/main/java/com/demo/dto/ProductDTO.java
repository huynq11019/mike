package com.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ProductDTO {

    private String id;

    private String productName;

    private String thumbNail;

    private String subCateName;

    private Long price;

    private float discount;

    private Long supplierId;

    private Long vendorId;

    private List<String> images;
}
