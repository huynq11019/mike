package com.demo.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.entity.UserLogin;
import com.demo.repository.UserLoginRepository;
import com.demo.service.UserLoginService;
@Service
public class UserLoginServiceImpl implements UserLoginService {
	@Autowired
	private UserLoginRepository loginRepository;

	@Override
	public UserLogin save(UserLogin loginLog) {
		  return this.loginRepository.save(loginLog);
	}
}
