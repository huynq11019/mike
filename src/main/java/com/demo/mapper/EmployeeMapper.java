package com.demo.mapper;

import com.demo.controller.protoco.request.EmployeeRequest;
import com.demo.controller.protoco.response.EmployeeResponse;
import com.demo.dto.EmployeeDTO;
import com.demo.entity.Employee;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EmployeeMapper extends BaseMapper<EmployeeDTO, Employee> {
    // request to entity
    Employee requestToEntity(EmployeeRequest request);

    // DTO to response
    EmployeeResponse dtoToEntity(EmployeeDTO dto);

    // Request to DTO
    EmployeeDTO pathFromRequestToModel(EmployeeRequest employeeRequest);
}
