package com.demo.repository.custom;

import java.util.List;

import com.demo.entity.Account;

public interface AccountRepositoryCustom {
	List<Account> getList();

}
