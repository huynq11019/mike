package com.demo.repository.custom;

import java.awt.print.Pageable;
import java.util.List;

import org.springframework.util.MultiValueMap;

import com.demo.entity.Employee;

public interface EmployeeRepositoryCustom {
	
	public List<Employee> findUserByRoleId(Long roleId);
	
	public List<Employee> findUserInfo(MultiValueMap<String, Object> queryParams, Pageable pageable);
	
	Long countUserInfo(MultiValueMap<String, Object> queryParams);
	
	List<Employee> searchUsers(MultiValueMap<String, String> queryParams, Pageable pageable);
	
	List<Employee> fuzzySearchByFullnameAndEmail(String searchTerm);
	
	List<Employee> getListEmployeeByRole(Integer roleId);
}
