package com.demo.repository;

import com.demo.entity.AccessToken;
import com.demo.repository.custom.AccessTokenRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Optional;

public interface AccessTokenRepository extends JpaRepository<AccessToken, Long>, AccessTokenRepositoryCustom {

    /**
     * Delete by expired time less than equal or expired.
     *
     * @param expiredInstant
     *            the expired instant
     * @param expired
     *            the expired
     * @return the long
     */
    long deleteByExpiredTimeLessThanEqualOrExpired(Instant expiredInstant, boolean expired);

    /**
     * Find by username and value.
     *
     * @param username
     *            the username
     * @param value
     *            the value
     * @return the optional
     */
    Optional<AccessToken> findByUsernameAndValue(String username, String value);

    /**
     * Find by value.
     *
     * @param value
     *            the value
     * @return the optional
     */
    Optional<AccessToken> findByValue(String value);

    /**
     * Find by token id.
     *
     * @param tokenId
     *            the token id
     * @return the optional
     */
    Optional<AccessToken> findByTokenId(Long tokenId);
}
